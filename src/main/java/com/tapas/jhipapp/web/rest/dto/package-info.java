/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.tapas.jhipapp.web.rest.dto;
